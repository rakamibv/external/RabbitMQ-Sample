﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;

namespace Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Thread.Sleep(5 * 1000);
                
                var factory = new ConnectionFactory() { HostName = "localhost", Port = 5672, UserName = "rabbitmq", Password = "rabbitmq" };
                using(var connection = factory.CreateConnection())
                using(var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "test",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    string message = $"Hello World! { DateTime.Now }";
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "",
                        routingKey: "test",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine("PUBLISHED MESSAGE");
                }
            }
        }
    }
}